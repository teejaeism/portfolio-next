import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import {FaChalkboardTeacher,FaRobot,FaCode,FaStar} from 'react-icons/fa';
import {GiTeacher} from 'react-icons/gi';
import { SiJavascript,SiNextDotJs,SiBootstrap } from "react-icons/si";
import { MdSchool } from "react-icons/md";

export default function timeline(){

  return (
    <>
    <div className="mt-5" id="timeline">
      <h1 className="text-white text-center mt-5 mt-md-1">Education and Experience</h1>
      <VerticalTimeline className="d-none d-md-block">
        <VerticalTimelineElement
          className="vertical-timeline-element--education"
          contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
          contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
          iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
          icon={<MdSchool  />}
        >
          <span className="mt-5">May 2012</span>
          <h3 className="vertical-timeline-element-title">Bachelor Of Secondary Education Major in English</h3>
          <h4 className="vertical-timeline-element-subtitle">Mendiola, Manila</h4>
          <p>
            Graduated from Centro Escolar University
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--education"
          contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
          contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
          iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
          icon={<MdSchool  />}
        >
          <span className="mt-5">September 2012</span>
          <h3 className="vertical-timeline-element-title">Licensure Examination for Teachers</h3>
          <h4 className="vertical-timeline-element-subtitle">Manila</h4>
          <p>
            Passed the licensure examination for teachers in 2012
          </p>
        </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaChalkboardTeacher  />}
          >
            <span className="mt-5">September 2013 - May 2016</span>
            <h3 className="vertical-timeline-element-title">Faculty Member</h3>
            <h4 className="vertical-timeline-element-subtitle">Taif, Saudi Arabia</h4>
            <p>
              Faculty Member at Shorouq Al Mamlakah International School in Saudi Arabia.
            </p>
            <p>
              Grade Levels Taught: Grade 3-10
            </p>
            <p>
              Subjects Taught: English,MAPEH,Civil Sciences,Creative Writing, Campus Journalism
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaRobot  />}
          >
            <span className="mt-5">February 2018 - August 2018</span>
            <h3 className="vertical-timeline-element-title">Robotics Educator</h3>
            <h4 className="vertical-timeline-element-subtitle">Greenhills, San Juan</h4>
            <p>
              Taught robotics using Lego Mindstorms NXT and EV3 for learners aged 9-12 through First Robotics Learning Center (FRLC)
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            dateClassName="opacity"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<GiTeacher  />}
          >
            <span className="mt-5">September 2018 - March 2019</span>
            <h3 className="vertical-timeline-element-title">English as Second Language Teacher</h3>
            <h4 className="vertical-timeline-element-subtitle">Hai Phong, Vietnam</h4>
            <p>
             Taught English as Second Language to students in Hai Phong Vietnam through CTTN Senbol Education (Senbol)
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaCode />}
          >
            <span className="mt-5">January 2020 - October 2021</span>
            <h3 className="vertical-timeline-element-title">Jr. Instructor</h3>
            <h4 className="vertical-timeline-element-subtitle">Quezon City, Metro Manila</h4>
            <p>
             Taught MERN Stack Web Development Course as Jr. Instructor at Zuitt Coding Bootcamp.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<SiJavascript />}
          >
            <span className="mt-5">October 2021 - Present</span>
            <h3 className="vertical-timeline-element-title">Sr. Instructor</h3>
            <h4 className="vertical-timeline-element-subtitle">Quezon City, Metro Manila</h4>
            <p>
             Currently teaching Web Development through Zuitt Coding Bootcamp
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
            icon={<FaStar/>}
          />

      </VerticalTimeline>

      <VerticalTimeline className="d-block d-md-none" animate={false}>
        <VerticalTimelineElement
          className="vertical-timeline-element--education"
          contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
          contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
          iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
          icon={<MdSchool  />}
        >
          <span className="mt-5">May 2012</span>
          <h3 className="vertical-timeline-element-title">Bachelor Of Secondary Education Major in English</h3>
          <h4 className="vertical-timeline-element-subtitle">Mendiola, Manila</h4>
          <p>
            Graduated from Centro Escolar University
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--education"
          contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
          contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
          iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
          icon={<MdSchool  />}
        >
          <span className="mt-5">September 2012</span>
          <h3 className="vertical-timeline-element-title">Licensure Examination for Teachers</h3>
          <h4 className="vertical-timeline-element-subtitle">Manila</h4>
          <p>
            Passed the licensure examination for teachers in 2012
          </p>
        </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaChalkboardTeacher  />}
          >
            <span className="mt-5">September 2013 - May 2016</span>
            <h3 className="vertical-timeline-element-title">Faculty Member</h3>
            <h4 className="vertical-timeline-element-subtitle">Taif, Saudi Arabia</h4>
            <p>
              Faculty Member at Shorouq Al Mamlakah International School in Saudi Arabia.
            </p>
            <p>
              Grade Levels Taught: Grade 3-10
            </p>
            <p>
              Subjects Taught: English,MAPEH,Civil Sciences,Creative Writing, Campus Journalism
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaRobot  />}
          >
            <span className="mt-5">February 2018 - August 2018</span>
            <h3 className="vertical-timeline-element-title">Robotics Educator</h3>
            <h4 className="vertical-timeline-element-subtitle">Greenhills, San Juan</h4>
            <p>
              Taught robotics using Lego Mindstorms NXT and EV3 for learners aged 9-12 through First Robotics Learning Center (FRLC)
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            dateClassName="opacity"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<GiTeacher  />}
          >
            <span className="mt-5">September 2018 - March 2019</span>
            <h3 className="vertical-timeline-element-title">English as Second Language Teacher</h3>
            <h4 className="vertical-timeline-element-subtitle">Hai Phong, Vietnam</h4>
            <p>
             Taught English as Second Language to students in Hai Phong Vietnam through CTTN Senbol Education (Senbol)
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaCode />}
          >
            <span className="mt-5">January 2020 - October 2021</span>
            <h3 className="vertical-timeline-element-title">Jr. Instructor</h3>
            <h4 className="vertical-timeline-element-subtitle">Quezon City, Metro Manila</h4>
            <p>
             Taught MERN Stack Web Development Course as Jr. Instructor at Zuitt Coding Bootcamp.
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgba(0, 0, 0, 0.5)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgba(0, 0, 0, 0.5)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<SiJavascript />}
          >
            <span className="mt-5">October 2021 - Present</span>
            <h3 className="vertical-timeline-element-title">Sr. Instructor</h3>
            <h4 className="vertical-timeline-element-subtitle">Quezon City, Metro Manila</h4>
            <p>
             Currently teaching Web Development through Zuitt Coding Bootcamp
            </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
            icon={<FaStar/>}
          />
      </VerticalTimeline>
    </div>

    </>

    )

}

