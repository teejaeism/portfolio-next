import { GiTechnoHeart } from "react-icons/gi";
import { FaFacebookSquare, FaInstagramSquare, FaTwitterSquare,FaArrowCircleDown, FaPhone, FaGitlab } from "react-icons/fa";
import { AiOutlineMail, AiTwotoneMail } from "react-icons/ai";
import Link from 'next/link'

export default function footer(){
	return (

			<footer className="d-flex align-items-center justify-content-center mt-3 p-2">
				<div className="text-center my-2">
					<p>
					    <Link href="https://www.facebook.com/teejaeism/">
							  <span className="mx-2">
							      <FaFacebookSquare size={30}/>
							  </span>
					    </Link>
					    <Link href="https://twitter.com/teejaeism">
							  <span className="mx-2">
							       <FaTwitterSquare size={30}/>
							  </span>
					    </Link>
					    <Link href="https://www.instagram.com/crackinfingers/">
							  <span className="mx-2">
							      <FaInstagramSquare size={30}/>            
							  </span>
					    </Link>
					    <Link href="https://gitlab.com/teejaeism/">
							  <span className="mx-2">
							      <FaGitlab size={30}/>
							  </span>
					    </Link>
					</p>
				</div>
			</footer>

		)

}