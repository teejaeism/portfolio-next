import {Card,Button} from 'react-bootstrap'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { DiHtml5 } from "react-icons/di";
import { DiCss3Full } from "react-icons/di";
import { DiJavascript1 } from "react-icons/di";
import { DiPhp } from "react-icons/di";
import { DiStreamline} from "react-icons/di";
import { DiLaravel } from "react-icons/di";
import { DiBootstrap } from "react-icons/di";
import { DiMongodb } from "react-icons/di";
import { FaNodeJs } from "react-icons/fa";
import { RiReactjsLine } from "react-icons/ri";

export default function projects(){

	const responsive = {
	  desktop: {
	    breakpoint: { max: 3000, min: 1024 },
	    items: 1,
	    slidesToSlide: 1 // optional, default to 1.
	  },
	  tablet: {
	    breakpoint: { max: 1024, min: 464 },
	    items: 1,
	    slidesToSlide: 1 // optional, default to 1.
	  },
	  mobile: {
	    breakpoint: { max: 464, min: 0 },
	    items: 1,
	    slidesToSlide: 1 // optional, default to 1.
	  }
	};

	return (

		<div className="my-5" id="projects">
		<h1 className="display-3 mt-2 text-white text-center">My Projects</h1>
		<div className="d-none d-md-block">
			<Carousel className="my-5 text-center" infinite={true} responsive={responsive}>
				<div className="col-12 col-lg-8 mx-auto" id="overlayProjOn1">
					<img src="./Geek.png" className="img-fluid"/>
					<div className="text-white p-md-5" id="overlayProj1">
						<DiHtml5 /> <DiCss3Full /> <DiJavascript1 /><DiBootstrap />
						<h3>Geekiversity</h3>
						<p className="px-5 pb-1 d-none d-md-block">Geekiversity is a blog-style project created using HTML5, CSS and Javascript and Bootstrap. It is a static website which features articles 
						and blogs about Anime, wherein the author/editor of the blog would feature and display 6 of his favorite animes every week.</p>
						<a href="https://teejaeism.gitlab.io/capstone1/index.html" target="_blank" className="btn btn-outline-light">Check It Out!</a>
					</div>
				</div>
				<div className="col-12 col-lg-8 mx-auto" id="overlayProjOn2">
					<img src="./Game.png" className="img-fluid"/>
					<div id="overlayProj2" className="text-white p-md-5">
						<DiHtml5 /> <DiCss3Full /> <DiJavascript1 /> <DiBootstrap /> <DiPhp /> <DiLaravel /> <DiStreamline />
						<h3>GameCity</h3>
						<p className="px-5 pb-1 d-none d-md-block">GameCity is a dynamic project created using mostly Laravel-PHP Framework. It is a dynamic website which allows users to login and register
						and be able to rent out and return Video Games or Tabletop Games. It also allows an Admin user to add, edit and delete products at will.</p>
						<a href="http://gamecity.herokuapp.com" target="_blank" className="btn btn-outline-light">Check It Out!</a>
					</div>
				</div>
				<div className="col-12 col-lg-8 mx-auto" id="overlayProjOn3">
					<img src="./Nota.png" className="img-fluid"/>
					<div id="overlayProj">
						<div id="overlayProj3" className="text-white p-md-5">
						<DiHtml5 /><DiCss3Full /> <DiJavascript1 /> <DiBootstrap /><FaNodeJs /><RiReactjsLine /><DiMongodb />
						<h3>Nota Place</h3>
						<p className="px-5 pb-1 d-none d-md-block">Nota Place is a dynamic project created using  MongoDB, ExpressJS, ReactJs and NodeJS. It is a dynamic website which allows users to login and register
						and be able to book and schedule Karaoke Rooms as well as add Food and Beverage to their orders. It also allows an Admin user to add, edit and delete Rooms and Food at will.</p>
						<a href="https://modest-mcnulty-5829fd.netlify.app" target="_blank" className="btn btn-outline-light">Check It Out!</a>
						</div>			
					</div>
				</div>
			</Carousel>
		</div>
		<div className="d-block d-md-none">
			<Carousel className="my-5 text-center" infinite={true} responsive={responsive}>
				<div className="col-12 mx-auto">
					<Card className="projCards">
					  <Card.Img variant="top" src="./Geek.png" />
					  <Card.Body>
					    <Card.Title>Geekiversity</Card.Title>
					    <Card.Text>
					    	<DiHtml5 /> <DiCss3Full /> <DiJavascript1 /> <DiBootstrap />
					    </Card.Text>
					    <Card.Text>
						Geekiversity is a blog-style project created using HTML5, CSS and Javascript and Bootstrap. It is a static website which features articles 
						and blogs about Anime, wherein the author/editor of the blog would feature and display 6 of his favorite animes every week.
					   </Card.Text>
					   <a href="https://teejaeism.gitlab.io/capstone1/index.html" target="_blank" className="btn btn-dark btn-block">Check It Out!</a>
					  </Card.Body>
					</Card>
				</div>
				<div className="col-12 mx-auto">
					<Card className="projCards">
					  <Card.Img variant="top" src="./Game.png" />
					  <Card.Body>
					    <Card.Title>Game City</Card.Title>
					    <Card.Text>
					    	<DiHtml5 /> <DiCss3Full /> <DiJavascript1 /> <DiBootstrap /> <DiPhp /> <DiLaravel /> <DiStreamline />
					    </Card.Text>
					    <Card.Text>
					    GameCity is a dynamic project created using mostly Laravel-PHP Framework. It is a dynamic website which allows users to login and register
					    and be able to rent out and return Video Games or Tabletop Games. It also allows an Admin user to add, edit and delete products at will.
					   </Card.Text>
					   <a href="http://gamecity.herokuapp.com" target="_blank" className="btn btn-dark btn-block">Check It Out!</a>
					  </Card.Body>
					</Card>
				</div>
				<div className="col-12 mx-auto">
					<Card className="projCards">
					  <Card.Img variant="top" src="./Nota.png" />
					  <Card.Body>
					    <Card.Title>Nota Place</Card.Title>
					    <Card.Text>
					    	<DiHtml5 /><DiCss3Full /> <DiJavascript1 /> <DiBootstrap /><FaNodeJs /><RiReactjsLine /><DiMongodb />
					    </Card.Text>
					    <Card.Text>
						    Nota Place is a dynamic project created using  MongoDB, ExpressJS, ReactJs and NodeJS. It is a dynamic website which allows users to login and register
						    and be able to book and schedule Karaoke Rooms as well as add Food and Beverage to their orders. It also allows an Admin user to add, edit and delete Rooms and Food at will.
					   </Card.Text>
					   <a href="http://gamecity.herokuapp.com" target="_blank" className="btn btn-dark btn-block">Check It Out!</a>
					  </Card.Body>
					</Card>
				</div>
			</Carousel>
		</div>
	</div>

		)

}