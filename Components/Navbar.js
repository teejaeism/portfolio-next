import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
//import nextJS Link component for client-side navigation
import Link from 'next/link'


export default function NavBar() {
    return (
        <Navbar variant="dark" expand="lg" className="mb-md-5">
            <Link href="/">
                <a className="navbar-brand d-lg-none">TJC</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mx-auto text-center">

                            <Link href="/" className="">
                                <a className="navbar-brand d-none d-lg-block">TJC</a>
                            </Link>
                            <Link href="#timeline">
                                <a className="nav-link ml-lg-auto" role="button">Resume</a>
                            </Link>
                            <Link href="#skills">
                                <a className="nav-link mr-lg-auto" role="button">Skills</a>
                            </Link>
                            <Link href="#projects">
                                <a className="nav-link mr-lg-auto" role="button">Projects</a>
                            </Link>

                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}