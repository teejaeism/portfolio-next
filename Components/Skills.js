import { FaGitSquare, FaHtml5,FaCss3Alt,FaReact } from "react-icons/fa";
import { SiJavascript,SiNextDotJs,SiBootstrap } from "react-icons/si";
import {Card,Row,Col} from 'react-bootstrap';
export default function skills(){

	return (
		<>
			<div className="mt-4" id="skills">
				<h1 className="text-white text-center my-5">Front-End Development</h1>
				<Row className="my-4">
					<Col xs={6} md={2}>
						<Card className="p-3 text-white mr-1  my-2 mt-md-1" id="overlaySkillOn1">
						  <Card.Img src="./html.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill1">
						    <Card.Text className="text-center">HyperText Markup Language (HTML)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2}>
						<Card className="p-3 text-white mx-1  my-2 mt-md-1" id="overlaySkillOn2">
						  <Card.Img src="./css4.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill2">
						    <Card.Text className="text-center">Cascading Style Sheets (CSS)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2}>
						<Card className="p-3 text-white mx-1 my-2 mt-md-1" id="overlaySkillOn3">
						  <Card.Img src="./javascript.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill3" className="py-md-5">
						    <Card.Text className="text-center my-1">Javascript (JS)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2}>
						<Card className="p-3 text-white mx-1 my-2 mt-md-1" id="overlaySkillOn4">
						  <Card.Img src="./bot.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill4" className="py-md-5">
						    <Card.Text className="text-center my-1">BootStrap (BS)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2}>
						<Card className="p-3 text-white mx-1 py-2 my-2 mt-md-1" id="overlaySkillOn5">
						  <Card.Img src="./react.jpg" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill5" className="py-md-5">
						    <Card.Text className="text-center my-1">ReactJS (React)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn6">
						<Card className="p-3 text-white ml-1 my-2 mt-md-1">
						  <Card.Img src="./next.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill6" className="py-md-5">
						    <Card.Text className="text-center my-1">NextJS (Next)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
				</Row>
				<h1 className="text-white text-center my-5">Back-End Development</h1>
				<Row className="my-4">
					<Col xs={6} md={2} id="overlaySkillOn7">
						<Card className="p-3 text-white mx-1 px-1 my-2 mt-md-1">
						  <Card.Img src="./nodejs.jpg" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill7" className="py-md-5">
						    <Card.Text className="text-center my-1">NodeJS (Node)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn8">
						<Card className="p-3 text-white mx-1 py-2 px-1 my-2 mt-md-1">
						  <Card.Img src="./ExpressJS1.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill8" className="py-md-5">
						    <Card.Text className="text-center my-1">ExpressJS (Express)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn9">
						<Card className="p-3 text-white mx-1 py-2 px-1 my-2 mt-md-1">
						  <Card.Img src="./laravel1.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill9" className="py-md-5">
						    <Card.Text className="text-center my-1">Laravel-PHP (Laravel)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn10">
						<Card className="p-3 text-white mx-1 my-2 mt-md-1">
						  <Card.Img src="./php.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill10" className="py-md-4">
						    <Card.Text className="text-center my-1">Hypertext Preprocessor (PHP)</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn11">
						<Card className="p-3 text-white mx-1 my-2 mt-md-1">
						  <Card.Img src="./mongodb1.png" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill11" className="py-md-2">
						    <Card.Text className="text-center my-1">MongoDB (Mongo) NoSQL Database</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
					<Col xs={6} md={2} id="overlaySkillOn12">
						<Card className="p-3 bg-light text-white mx-1 my-2 mt-md-1">
						  <Card.Img src="./mysql.svg" alt="Card image" className="img-fluid" />
						  <Card.ImgOverlay id="overlaySkill12" className="py-md-5">
						    <Card.Text className="text-center my-1">MySQL (SQL) SQL Database</Card.Text>
						  </Card.ImgOverlay>
						</Card>
					</Col>
				</Row>
			</div>
		</>




		)

}