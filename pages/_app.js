import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {useEffect} from 'react'
import {Container} from 'react-bootstrap'
import Navbar from '../Components/Navbar'
import Footer from '../Components/Footer'
import BackToTop from '../Components/BackToTop'
import Head from 'next/head'
import 'animate.css'
function MyApp({ Component, pageProps }) {

  return( 
  	<>	
  		<Head>
  			<link rel="preconnect" href="https://fonts.gstatic.com"/>
  			<link href="https://fonts.googleapis.com/css2?family=Lora&family=Montserrat:wght@600&display=swap" rel="stylesheet"/>
  		</Head>
  		<Navbar />
	  	<Container>
	  		<Component {...pageProps} />
	  		<BackToTop /> 
	  	</Container>
	  	<Footer />
  	</>


  	)
}

export default MyApp
