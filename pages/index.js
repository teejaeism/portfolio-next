import {Jumbotron,Row,Col,Carousel} from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import { FaFacebookSquare, FaInstagramSquare, FaTwitterSquare,FaArrowCircleDown, FaPhone, FaGitlab } from "react-icons/fa";
import { AiOutlineMail, AiTwotoneMail } from "react-icons/ai";
import Projects from '../Components/Projects'
import Skills from '../Components/Skills'
import Timeline from '../Components/Timeline'
export default function index(){
  return (
    <>
      <Head>
        <title>Tee Jae Calinao || Portfolio</title>
      </Head>
      <Row className="mt-5 mx-auto text-white" id="tjLanding">
            <Col xs={10} md={3} className="mx-auto">
              <img src="./me.jpg" alt="tj" className="img-fluid" id="tj"/>
            </Col>
            <Col xs={12} md={12} className="mt-3 d-none d-md-block text-center">
              <h1>Tee Jae Bengan Calinao</h1>
              <Carousel controls={false} interval={2200} indicators={false}>
                <Carousel.Item>
                  <h3>Professional Teacher</h3>
                </Carousel.Item>
                <Carousel.Item>
                  <h3>Full-Stack Web Developer</h3>
                </Carousel.Item>
                <Carousel.Item>
                  <h3>Aspiring Writer</h3>
                </Carousel.Item>
              </Carousel>
              <p><FaPhone size={15}/>  09266772400 | <AiTwotoneMail size={15}/>  teejaeism@gmail.com</p>
              <p>A teacher into 9 years of experience, Now a full-stack developer ready to take on the world.</p>
            </Col> 
            <Col xs={12} className="d-md-none text-center mt-3">
              <h3>Tee Jae Calinao</h3>
              <Carousel controls={false} interval={1000} indicators={false}>
                <Carousel.Item>
                  <h6>Professional Teacher</h6>
                </Carousel.Item>
                <Carousel.Item>
                  <h6>Full-Stack Web Developer</h6>
                </Carousel.Item>
                <Carousel.Item>
                  <h6>Aspiring Writer</h6>
                </Carousel.Item>
              </Carousel>
              <div>
                <p className="my-0"><FaPhone size={15}/> 09266772400</p>
                <p className="my-0"> <AiTwotoneMail size={15}/> teejaeism@gmail.com</p>
                <p className="my-1">A teacher into 9 years of experience, Now a full-stack developer ready to take on the world.</p>
              </div>
            </Col>  
      </Row>
      <div className="text-center text-white my-3">
        <p>
            <Link href="https://www.facebook.com/teejaeism/">
              <span className="mx-2">
                  <FaFacebookSquare size={30}/>
              </span>
            </Link>
            <Link href="https://twitter.com/teejaeism">
              <span className="mx-2">
                   <FaTwitterSquare size={30}/>
              </span>
            </Link>
            <Link href="https://www.instagram.com/crackinfingers/">
              <span className="mx-2">
                  <FaInstagramSquare size={30}/>            
              </span>
            </Link>
            <Link href="https://gitlab.com/teejaeism/">
              <span className="mx-2">
                  <FaGitlab size={30}/>
              </span>
            </Link>
        </p>
      </div>
      <Timeline />
      <Skills />
      <Projects />
    </>

    )
}